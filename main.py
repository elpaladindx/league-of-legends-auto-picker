import cv2 as cv
import numpy as np
from time import time, sleep
from windowcapture import WindowCapture
from vision import Vision
import pyautogui
import tkinter as tk
from tkinter import simpledialog

ROOT = tk.Tk()

ROOT.withdraw()
# Name of the window to capture
windowToCapture = simpledialog.askstring(title="Captura", prompt="Ventana a Capturar:")
# Champ to ban
champBanChoise = simpledialog.askstring(title='Captura', prompt='Baneo:')
# Champ to pick
ChampPickChoise = simpledialog.askstring(title='Captura', prompt='Pick:')

# Automate the user Input
def selectedInput(userInput):
    pyautogui.write(userInput, interval=0.25)

# Automate Champion Management
def actionSelection(value, value2, value3):
    sleep(5)
    targets = vision_input.get_click_points(rectangles)
    target = wincap.get_screen_position(targets[0])
    pyautogui.doubleClick(x=target[0], y=target[1])
    sleep(1)
    selectedInput(value)
    sleep(1)
    # Champion select position x=790 y=357
    pyautogui.click(790, 357)
    sleep(1)
    locktargets = value2.get_click_points(value3)
    locktarget = wincap.get_screen_position(locktargets[0])
    pyautogui.click(x=locktarget[0], y=locktarget[1])

# initialize the WindowCapture class
if len(windowToCapture) > 1:
    wincap = WindowCapture(windowToCapture)
else:
    wincap = WindowCapture(None)

# Detect Accept Button
#vision_input = Vision('aceptar.jpg')
vision_input = Vision('rechazar.jpg')
# Detect if in queue
vision_queue = Vision('queue.jpg')

print('Im up')

#loop_time = time()
while(True):
    # Detect if it worked
    flag = 0

    # get an updated image of the game
    screenshot = wincap.get_screenshot()

    # do object detection
    rectangles = vision_input.find(screenshot, 0.7)

    # draw the detection results onto the original image
    #output_image = vision_input.draw_rectangles(screenshot, rectangles)

    # Debug Screen Capture
    #cv.imshow('Matches', output_image)

    # Detect if in queue
    detectqueue = vision_queue.find(screenshot, 0.7)

    if len(rectangles) > 0 and flag == 0:
        targets = vision_input.get_click_points(rectangles)
        target = wincap.get_screen_position(targets[0])
        pyautogui.moveTo(x=target[0], y=target[1])
        sleep(7)
        pyautogui.click()
        print('i did a click!')
        sleep(1)
        #debug kill screen
        #cv.destroyAllWindows()
        # Detect if not in queue
        if len(detectqueue) == 0:
            print('not on queue')
            flag = 1
    
    # Detect if in queue
    if len(detectqueue) > 0:
        print('Still in queue')

    # if not in queue, quit this loop
    if flag == 1:
        break

    # debug the loop rate
    #print('FPS {}'.format(1 / (time() - loop_time)))
    #loop_time = time()

    # press 'q' with the output window focused to exit.
    # waits 1 ms every loop to process key presses
    if cv.waitKey(1) == ord('q'):
        # Kill Debug Screencapture
        cv.destroyAllWindows()
        break

# Detect Search Box
vision_search = Vision('search.jpg')
# Detect if is Ban Face
vision_champBan = Vision('champBan.jpg') 
# Detect if is Selection Face
vision_champSelec = Vision('champSelector.jpg')
# Detect Lock button
vision_lock = Vision('lock.jpg')
# Detect Block Button
vision_block = Vision('block.jpg')

while(True):
    # Flag to determine current Phase
    flag = 0
    
    # get an updated image of the game
    screenshot = wincap.get_screenshot()

    # Find Search Button
    rectangles = vision_search.find(screenshot, 0.7)

    # Find Lock Button
    lockRectangles = vision_lock.find(screenshot, 0.7)
    # Find Block Button
    blockRectangles = vision_block.find(screenshot, 0.7)

    # Find Ban Face
    isBanFace = vision_champBan.find(screenshot, 0.7)
    # Find Selection Face
    isSelectionFace = vision_champSelec.find(screenshot, 0.7)

    # Detect Phase
    if flag == 0:
        if len(isBanFace) > 0:
            flag = 1
        elif len(isSelectionFace) > 0:
            flag = 2

    # Ban Champion Face
    if len(rectangles) > 0 and flag == 1:
        print('Im banning: ', champBanChoise)
        sleep(5)
        actionSelection(champBanChoise, vision_block, blockRectangles)
        flag = 0
        cv.destroyAllWindows()

    # Pick Champion Face
    if len(rectangles) > 0 and flag == 2:
        print('Im Picking: ', ChampPickChoise)
        sleep(5)
        actionSelection(ChampPickChoise, vision_lock, lockRectangles)
        cv.destroyAllWindows()
        break

    # Force kill task
    if cv.waitKey(1) == ord('q'):
        cv.destroyAllWindows()
        break

# Need to determing lane
# Auto Runes using Champ Name
# Auto Spells using input and if JG select smite

print('Done.')